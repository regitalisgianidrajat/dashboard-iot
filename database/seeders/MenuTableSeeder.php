<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'menu_name' => 'Manage Users',
                'menu_url' => 'user'
               ),
            array(
                'menu_name' => 'Activity Log',
                'menu_url' => 'activity-log'
                ),
            array(
                'menu_name' => 'Privileges',
                'menu_url' => 'manage-privileges'
                ),
            );
        \App\Models\Admin\MenuModel::insert($data);
    }
}
